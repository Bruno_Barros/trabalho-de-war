/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package soldados;


public class Cavaleiro extends Exercito {

    private static int SDano = 0, SVida = 0, SResistencia = 0, SMotivacao = 0, numeroChance = 10;

    public void Cavaleiro() {
       setDano(60);
       setVida(150);
       setResistencia(100);
        
    }
public void atribui(){
        setDano(60+SDano);
       setVida(150+SVida);
       setResistencia(100+SResistencia);
       setMotivacao(SMotivacao);
    }
    public static int getSDano() {
        return SDano;
    }

    public static void setSDano(int SDano) {
        Cavaleiro.SDano = SDano;
    }

    public static int getSVida() {
        return SVida;
    }

    public static void setSVida(int SVida) {
        Cavaleiro.SVida = SVida;
    }

    public static int getSResistencia() {
        return SResistencia;
    }

    public static void setSResistencia(int SResistencia) {
        Cavaleiro.SResistencia = SResistencia;
    }

    public static int getSMotivacao() {
        return SMotivacao;
    }

    public static void setSMotivacao(int SMotivacao) {
        Cavaleiro.SMotivacao = SMotivacao;
    }

    public static int getNumeroChance() {
        return numeroChance;
    }

    public static void setNumeroChance(int numeroChance) {
        Cavaleiro.numeroChance = numeroChance;
    }
}
