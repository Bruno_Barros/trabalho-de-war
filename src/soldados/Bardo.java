/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package soldados;

/**
 *
 * @author Aluno
 */
public class Bardo extends Exercito{
    
     private static int SDano=0,SVida=0,SResistencia=0,SMotivacao=0,numeroChance=10;
    
    public void Bardo(){
       setDano(20);
       setVida(60);
       setResistencia(30);
    
    }
    public void atribui(){
        setDano(20+SDano);
       setVida(60+SVida);
       setResistencia(30+SResistencia);
       setMotivacao(SMotivacao);
    }
    
    public static int getSDano() {
        return SDano;
    }

    public static void setSDano(int SDano) {
        Bardo.SDano = SDano;
    }

    public static int getSVida() {
        return SVida;
    }

    public static void setSVida(int SVida) {
        Bardo.SVida = SVida;
    }

    public static int getSResistencia() {
        return SResistencia;
    }

    public static void setSResistencia(int SResistencia) {
        Bardo.SResistencia = SResistencia;
    }

    public static int getSMotivacao() {
        return SMotivacao;
    }

    public static void setSMotivacao(int SMotivacao) {
        Bardo.SMotivacao = SMotivacao;
    }

    public static int getNumeroChance() {
        return numeroChance;
    }

    public static void setNumeroChance(int numeroChance) {
        Bardo.numeroChance = numeroChance;
    }
}
