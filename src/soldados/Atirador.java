/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package soldados;


public class Atirador extends Exercito{
   
    
    private static int SDano=0,SVida=0,SResistencia=0,SMotivacao=0,numeroChance=10;

    public void Atirador() {
       setDano(100);
       setVida(80);
       setResistencia(30);
    }
    
    public void atribui(){
       setDano(100+SDano);
       setVida(80+SVida);
       setResistencia(30+SResistencia);
       setMotivacao(SMotivacao);
    }
  
    public static int getSMotivacao() {
        return SMotivacao;
    }

    public static void setSMotivacao(int SMotivacao) {
        Atirador.SMotivacao = SMotivacao;
    }
    public static int getSDano() {
        return SDano;
    }

    public static int getSVida() {
        return SVida;
    }

    public static int getSResistencia() {
        return SResistencia;
    }

    public static int getNumeroChance() {
        return numeroChance;
    }

    public static void setNumeroChance(int numeroChance) {
        Atirador.numeroChance = numeroChance;
    }
  

    public static void setSDano(int SDano) {
        Atirador.SDano = SDano;
    }

    public static void setSVida(int SVida) {
        Atirador.SVida = SVida;
    }

    public static void setSResistencia(int SResistencia) {
        Atirador.SResistencia = SResistencia;
    }
}
