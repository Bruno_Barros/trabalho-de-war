package trabalhodeprogramacao;

import soldados.Guerra;

public class mudaValores {

    private int contMadeira, contComida, contAgua, contGold;
    private static int madeira = 10000, comida = 10000, agua = 10000, gold = 10000;
    private boolean recursoAprovado;

    String muda, todos;

    public boolean verificaRecursoAtirador(int nTropa) {
        recursoAprovado = false;
        contMadeira = nTropa * 5;
        contComida = nTropa * 5;
        contAgua = nTropa * 10;
        contGold = nTropa * 50;

        tiraRecurso();

        return recursoAprovado;
    }

    public boolean verificaRecursoCavaleiro(int nTropa) {
        recursoAprovado = false;
        contMadeira = nTropa * 10;
        contComida = nTropa * 5;
        contAgua = nTropa * 20;
        contGold = nTropa * 30;

        tiraRecurso();

        return recursoAprovado;
    }

    public boolean verificaRecursoBardo(int nTropa) {
        recursoAprovado = false;
        contMadeira = nTropa * 5;
        contComida = nTropa * 5;
        contAgua = nTropa * 5;
        contGold = nTropa * 10;

        tiraRecurso();

        return recursoAprovado;
    }

    public boolean verificaRecursoGuerreiro(int nTropa) {
        recursoAprovado = false;
        contMadeira = nTropa * 10;
        contComida = nTropa * 10;
        contAgua = nTropa * 10;
        contGold = nTropa * 30;

        tiraRecurso();

        return recursoAprovado;
    }

    public void tiraRecurso() {
        if (contMadeira < madeira) {
            if (contComida < comida) {
                if (contAgua < agua) {
                    if (contGold < gold) {
                        recursoAprovado = true;
                        madeira -= contMadeira;
                        gold -= contGold;
                        agua -= contAgua;
                        comida -= contComida;
                    }
                }
            }
        }

    }

    public void resetDados(){
    
        madeira = 10000;
        comida = 10000;
        agua = 10000;
        gold = 10000;
        
       Guerra.setTodo(0);
       Guerra.setTodoA(0);
       Guerra.setTodoC(0);
       Guerra.setTodoG(0);
       Guerra.setTodoB(0);
        
        
    }
    public static int getMadeira() {
        return madeira;
    }

    public static void setMadeira(int madeira) {
        mudaValores.madeira = madeira;
    }

    public static int getComida() {
        return comida;
    }

    public static void setComida(int comida) {
        mudaValores.comida = comida;
    }

    public static int getAgua() {
        return agua;
    }

    public static void setAgua(int agua) {
        mudaValores.agua = agua;
    }

    public static int getGold() {
        return gold;
    }

    public static void setGold(int gold) {
        mudaValores.gold = gold;
    }

}
